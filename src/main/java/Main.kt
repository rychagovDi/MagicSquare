import data.Matrix
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * @author Rychagov D.I. on 03.12.2017.
 */
 
 
fun main(args: Array<String>) {

    Observable.just(Unit)
        .delay(1, TimeUnit.SECONDS)
        .repeat(10)
        .subscribeOn(Schedulers.computation())
        .observeOn(Schedulers.io())
        .subscribe{ /*updateView*/ }



  val matrix = Matrix(4)
    matrix.setText("Ghbdtn")
    matrix.encrypt()
    matrix.decrypt()
  }
