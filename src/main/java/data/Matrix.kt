package data

import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlin.concurrent.thread
import java.util.HashSet



/**
 * @author Rychagov D.I. on 29.11.2017.
 */


class Matrix(private val dimension: Int) {

  private val symbolsCount = dimension * dimension
  private val square = getSquare(dimension)

  var plainText: String = ""
    private set

  var cipherText: String = ""
    private set

  private val subject: PublishSubject<String> = PublishSubject.create()

  fun setText(plain: String) {
    plainText = plain

    while (plainText.length < symbolsCount) {
      plainText = plainText.plus("*")
    }

    plainText = plainText.take(symbolsCount)
  }

  fun encrypt() =
      square
          .flatten()
          .map { plainText[it - 1] }
          .fold("") { cipher, symbol -> cipher.plus(symbol) }
          .also { cipherText = it }
          .also { println(it) }

  fun decrypt() {
    subject
//        .distinct()
        .subscribeOn(Schedulers.computation())
        .observeOn(Schedulers.io())
        .subscribe { println(it) }

    permutationFinder(cipherText)
        .distinct()
        .forEach { println(it) }
  }

  //  private fun permuteString(begin: String, end: String) {
  //    if (end.length <= 1) {
  //      subject.onNext("$begin$end")
  //    } else {
  //      for (index in 1 until end.length) {
  //        val newString = end.substring(0, index) + end.substring(index + 1)
  //        permuteString(begin + end[index], newString)
  //      }
  //    }
  //  }

  private fun permuteString(count: Int, text: String) {
    if (count == symbolsCount) {
      subject.onNext(text)
    } else {
      for (i in count until symbolsCount) {
        permuteString(count + 1, swap(count, i, text))
      }
    }
  }

  private fun swap(first: Int, second: Int, text: String): String {
    return text.toMutableList().apply {
      set(first, text[second])
      set(second, text[first])
    }
        .fold("", String::plus)
  }

  fun permutationFinder(str: String): Set<String> {
    val perm = HashSet<String>()
    //Handling error scenarios
    if (str.isEmpty()) {
      perm.add("")
      return perm
    }
    val initial = str[0] // первый символ
    val rem = str.substring(1) // полная строка без первого символа
    val words = permutationFinder(rem)
    for (strNew in words) {
      (0..strNew.length).mapTo(perm) {
        charInsert(strNew, initial, it)
      }
    }
    return perm
  }

  fun charInsert(str: String, c: Char, j: Int): String {
    val begin = str.substring(0, j)
    val end = str.substring(j)
    return begin + c + end
  }

}
