package data

/**
 * @author Rychagov D.I. on 29.11.2017.
 */

fun square4x4() = listOf(
    listOf(16, 3, 2, 13),
    listOf(5, 10, 11, 8),
    listOf(9, 6, 7, 12),
    listOf(4, 15, 14, 1)
)

fun square5x5() = listOf(
    listOf(11, 24, 7, 20, 3),
    listOf(4, 12, 25, 8, 16),
    listOf(17, 5, 13, 21, 9),
    listOf(10, 18, 1, 14, 22),
    listOf(23, 6, 19, 2, 15)
)

fun square6x6() = listOf(
    listOf(27, 29, 2, 4, 13, 36),
    listOf(9, 11, 20, 22, 31, 18),
    listOf(32, 25, 7, 3, 21, 3),
    listOf(14, 16, 34, 30, 12, 5),
    listOf(28, 6, 15, 17, 26, 19),
    listOf(1, 24, 33, 35, 8, 10)
)

fun square7x7() = listOf(
    listOf(1, 38, 26, 14, 44, 32, 20),
    listOf(28, 9, 46, 34, 15, 3, 40),
    listOf(48, 29, 17, 5, 42, 23, 11),
    listOf(19, 7, 37, 25, 13, 43, 31),
    listOf(39, 27, 8, 45, 33, 21, 2),
    listOf(10, 47, 35, 16, 4, 41, 22),
    listOf(30, 18, 6, 36, 24, 12, 49)
)

fun square8x8() = listOf(
    listOf(64, 2, 3, 61, 60, 6, 7, 57),
    listOf(9, 55, 54, 12, 13, 51, 50, 16),
    listOf(17, 47, 46, 20, 21, 43, 42, 24),
    listOf(40, 26, 27, 37, 36, 30, 31, 33),
    listOf(32, 34, 35, 29, 28, 38, 39, 25),
    listOf(41, 23, 22, 44, 45, 19, 18, 48),
    listOf(49, 15, 14, 52, 53, 11, 10, 56),
    listOf(8, 58, 59, 5, 4, 62, 63, 1)
)

fun getSquare(dimension: Int): List<List<Int>> {
  return when(dimension) {
    4 -> square4x4()
    5 -> square5x5()
    6 -> square6x6()
    7 -> square7x7()
    8 -> square8x8()
    else -> emptyList()
  }
}